<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function seven_plus_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['seven_plus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Seven+ settings'),
  );
  $form['seven_plus']['seven_plus_disable_sticky_sidebar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable sticky sidebar'),
    '#description' => t("By default, the sidebar will fix itself when scrolling down a form. If you have a lot of fields in the sidebar, consider disabling the sticky sidebar to view them all."),
    '#default_value' => theme_get_setting('seven_plus_disable_sticky_sidebar', 'seven_plus'),
  );
  $form['seven_plus']['seven_plus_disable_sidebar_in_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable sidebar in forms'),
    '#description' => t("By default, the sidebar is enabled for forms."),
    '#default_value' => theme_get_setting('seven_plus_disable_sidebar_in_form', 'seven_plus'),
  );
  $form['seven_plus']['seven_plus_sidebar_field_ui'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display fields in the sidebar of the node edit form.'),
    '#description' => t("By default, each field is displayed in the main content area of the node edit form. This option allows you to move fields into the sidebar to improve user experience."),
    '#default_value' => theme_get_setting('seven_plus_sidebar_field_ui', 'seven_plus'),
    '#states' => array(
      'invisible' => array(
        ':input[name="seven_plus_disable_sidebar_in_form"]' => array('checked' => TRUE),
      ),
    ),
  );

  // If the sidebar is disabled, we need to disable the sidebar field ui as well.
  $seven_plus_disable_sidebar_in_form = theme_get_setting('seven_plus_disable_sidebar_in_form', 'seven_plus');
  if ($seven_plus_disable_sidebar_in_form == 1) {
    $form['seven_plus']['seven_plus_sidebar_field_ui']['#default_value'] = 0;
  }
  
  // Rebuild theme registry on form save.
  if (!empty($form_state)) {
    // Rebuild .info data.
    system_rebuild_theme_data();
    // Rebuild theme registry.
    drupal_theme_rebuild();
  }

}
