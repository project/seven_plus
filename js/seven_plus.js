/**
* Implementation of Drupal behavior.
*/
(function($) {
  Drupal.behaviors.seven_plus = {};
  Drupal.behaviors.seven_plus.attach = function(context, settings) {
    // Sticky sidebar functionality.
    var disableSticky = (settings.seven_plus !== undefined) ? settings.seven_plus.disable_sticky : false;
    if ($('#content .column-side .column-wrapper').length !== 0 ) {

      // Move fields to sidebar if it exists.
      var $sidebar_fields = $('.seven_plus_sidebar_field', context);
      var $sidebar = $('.column-side .column-wrapper');
      if ($sidebar.length && $sidebar_fields.length) {
        $sidebar_fields.once('seven_plus', function() {
          $sidebar.append($(this));
        });
        $('.column-main').removeClass('column-main-full');
      }

      // Check if the sidebar should be made sticky.
      if (!disableSticky) {
        var seven_plusColumn = $('#content .column-side .column-wrapper', context);
        if (seven_plusColumn && seven_plusColumn.offset()) {
          var seven_plusStickySidebar = seven_plusColumn.offset().top;
          $(window).scroll(function() {
            if ($(window).scrollTop() > seven_plusStickySidebar) {
              seven_plusColumn.each(function() {
                $(this).addClass("fixed");
                $(this).width($(this).parent().width());
              });
            }
            else {
              seven_plusColumn.each(function() {
                $(this).removeClass("fixed");
                $(this).width($(this).parent().width());
              });
            }
          });
        }
      }

    }

  };
})(jQuery);
