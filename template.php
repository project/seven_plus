<?php

/**
 * Implements hook_preprocess_html().
 */
function seven_plus_preprocess_html(&$vars) {
  // Disable sticky in the sidebar. Set option in JS
  $disable_sticky = theme_get_setting('seven_plus_disable_sticky_sidebar');
  drupal_add_js(array('seven_plus' => array('disable_sticky' => $disable_sticky)), array('type' => 'setting'));
}

/**
 * Implements hook_css_alter().
 */
function seven_plus_css_alter(&$css) {
  $seven_ie_files = array(
    path_to_theme() . '/ie.css' => drupal_get_path('theme', 'seven') . '/ie.css',
    path_to_theme() . '/ie7.css' => drupal_get_path('theme', 'seven') . '/ie7.css',
    path_to_theme() . '/ie6.css' => drupal_get_path('theme', 'seven') . '/ie6.css',
  );
  foreach ($seven_ie_files as $broken_key => $correct_key) {
    if (isset($css[$broken_key])) {
      $css[$correct_key] = $css[$broken_key];
      $css[$correct_key]['data'] = $correct_key;
      unset($css[$broken_key]);
    }
  }
}

/**
 * Implementation of hook_theme().
 */
function seven_plus_theme() {
  $items = array();

  if (!theme_get_setting('seven_plus_disable_sidebar_in_form')) {
    // Form layout: default (2 column).
    $items['block_add_block_form'] =
    $items['block_admin_configure'] =
    $items['comment_form'] =
    $items['contact_admin_edit'] =
    $items['contact_mail_page'] =
    $items['contact_mail_user'] =
    $items['filter_admin_format_form'] =
    $items['forum_form'] =
    $items['locale_languages_edit_form'] =
    $items['menu_edit_menu'] =
    $items['menu_edit_item'] =
    $items['node_type_form'] =
    $items['path_admin_form'] =
    $items['system_settings_form'] =
    $items['system_themes_form'] =
    $items['system_modules'] =
    $items['system_actions_configure'] =
    $items['taxonomy_form_term'] =
    $items['taxonomy_form_vocabulary'] =
    $items['user_profile_form'] =
    $items['user_admin_access_add_form'] = array(
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'seven_plus') .'/templates',
      'template' => 'form-default',
      'preprocess functions' => array(
        'seven_plus_preprocess_form_buttons',
      ),
    );

    // These forms require additional massaging.
    $items['confirm_form'] = array(
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'seven_plus') .'/templates',
      'template' => 'form-simple',
      'preprocess functions' => array(
        'seven_plus_preprocess_form_confirm'
      ),
    );
    $items['node_form'] = array(
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'seven_plus') .'/templates',
      'template' => 'form-default',
      'preprocess functions' => array(
        'seven_plus_preprocess_form_buttons',
        'seven_plus_preprocess_form_node',
      ),
    );
  }
  return $items;
}

/**
 * Preprocessor for theme('node_form').
 */
function seven_plus_preprocess_form_node(&$vars) {
  $vars['sidebar'] = isset($vars['sidebar']) ? $vars['sidebar'] : array();
  $map = array();
  // Support field_group if present.
  if (module_exists('field_group')) {
    $map += array(
      'group_sidebar' => 'sidebar',
      'group_footer' => 'footer',
    );
  }
  // Support nodeformcols if present.
  if (module_exists('nodeformcols')) {
    $map += array(
      'nodeformcols_region_right' => 'sidebar',
      'nodeformcols_region_footer' => 'footer',
      'nodeformcols_region_main' => NULL,
    );
  }
  if (isset($map)) {
    foreach ($map as $region => $target) {
      if (isset($vars['form'][$region])) {
        if (isset($vars['form'][$region]['#prefix'], $vars['form'][$region]['#suffix'])) {
          unset($vars['form'][$region]['#prefix']);
          unset($vars['form'][$region]['#suffix']);
        }
        if (isset($vars['form'][$region]['actions'], $vars['form'][$region]['actions'])) {
          $vars['actions'] = $vars['form'][$region]['actions'];
          unset($vars['form'][$region]['actions']);
        }
        if (isset($target)) {
          $vars[$target] = $vars['form'][$region];
          unset($vars['form'][$region]);
        }
      }
    }
  }
  // Default to showing taxonomy in sidebar if nodeformcols is not present.
  elseif (isset($vars['form']['taxonomy']) && empty($vars['sidebar'])) {
    $vars['sidebar']['taxonomy'] = $vars['form']['taxonomy'];
    unset($vars['form']['taxonomy']);
  }
}

/**
 * Helper function for cloning and drupal_render()'ing elements.
 */
function seven_plus_render_clone($elements) {
  static $instance;
  if (!isset($instance)) {
    $instance = 1;
  }
  foreach (element_children($elements) as $key) {
    if (isset($elements[$key]['#id'])) {
      $elements[$key]['#id'] = "{$elements[$key]['#id']}-{$instance}";
    }
  }
  $instance++;
  return drupal_render($elements);
}

function seven_plus_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $seven_plus_sidebar_field_ui = theme_get_setting('seven_plus_sidebar_field_ui', 'seven_plus');
  $seven_plus_disable_sidebar_in_form = theme_get_setting('seven_plus_disable_sidebar_in_form', 'seven_plus');
  if ($seven_plus_sidebar_field_ui == 1 && $seven_plus_disable_sidebar_in_form == 0) {
    $options = array(
      'default' => t('Default'),
      'seven_plus_sidebar_field' => t('Sidebar'),
    );
    $default = (isset($form_state['build_info']['args'][0]['seven_plus_edit_field_display'])) ? $form_state['build_info']['args'][0]['seven_plus_edit_field_display'] : 'default';
    $form['instance']['seven_plus_edit_field_display'] = array(
      '#type' => 'radios',
      '#title' => t('Set field display location'),
      '#description' => t('Choose where this field should be displayed.'),
      '#default_value' => $default,
      '#options' => $options,
    );
  }
}

function seven_plus_form_node_form_alter(&$form, $form_state) {
  $seven_plus_sidebar_field_ui = theme_get_setting('seven_plus_sidebar_field_ui', 'seven_plus');
  if ($seven_plus_sidebar_field_ui == TRUE) {
    if (isset($form_state['field']) && is_array($form_state['field'])) {
      foreach ($form_state['field'] AS $name => $field) {
        if (!isset($field[LANGUAGE_NONE]['instance'])) {
          continue;
        }
        if (isset($field[LANGUAGE_NONE]['instance']['seven_plus_edit_field_display'])) {
          $display = $field[LANGUAGE_NONE]['instance']['seven_plus_edit_field_display'];
          if ($display == 'seven_plus_sidebar_field') {
            $form[$name]['#attributes']['class'][] = 'seven_plus_sidebar_field';
          }
        }
      }
    }
  }
}

