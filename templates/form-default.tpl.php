<div class='form form-layout-default clearfix'>
  <div class='column-main<?php print empty($sidebar) ? ' column-main-full' : '';?>'><div class='column-wrapper clearfix'>
    <?php print drupal_render_children($form); ?>
  </div></div>
  <div class='js-column-side column-side'><div class='column-wrapper clearfix'>
    <?php print drupal_render($sidebar); ?>
  </div></div>
  <?php if (!empty($footer)): ?>
    <div class='column-footer'><div class='column-wrapper clearfix'><?php print drupal_render($footer); ?></div></div>
  <?php endif; ?>
</div>
